import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  constructor(public alertController: AlertController) {}

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Email Sent!',
      subHeader: 'your email has been sent',
      message: 'Please check your email to complete registration, PS: check your spam folder too',
      buttons: ['OK']
    });

    await alert.present();
  }

  ngOnInit() {
  }

}
