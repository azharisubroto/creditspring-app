import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Dashboard',
      url: '/login',
      icon: 'lock'
    },
    {
      title: 'Register',
      url: '/register',
      icon: 'checkmark-circle-outline'
    },
    {
      title: 'About CreditSpring',
      url: '/about',
      icon: 'contacts'
    },
    {
      title: 'How it Works',
      url: '/howitworks',
      icon: 'color-wand'
    },
    {
      title: 'Contact us',
      url: '/contact',
      icon: 'call'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
